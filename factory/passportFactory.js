const passport = require('passport');
const LocalStrategy = require('passport-local');
const User = require('../model/usermodel');
const AppError = require('../utils/appError');

passport.serializeUser(function (user, done) {
  done(null, user.email);
});

passport.deserializeUser(async function (email, done) {
  const user = await User.findOne({ $or: [{ username: email }, { email }] });
  if (!user)
    return done(new AppError('your not logged in plesae login', 401), false);
  done(null, user);
});

passport.use(
  'local',
  new LocalStrategy(async function (username, password, done) {
    const user = await User.findOne({
      $or: [{ email: username }, { username }],
    });
    if (!user || !user.isCorrectPassword(password, user.password))
      return done(null, false);
    return done(null, user);
  })
);
