const Joi = require('joi');
const passwordComplexity = require('joi-password-complexity');

const complexityOptions = {
  min: 5,
  max: 250,
  lowerCase: 1,
  upperCase: 1,
  numeric: 1,
  symbol: 1,
  requirementCount: 2,
};

exports.createUser = (req, res, next) => {
  const Schema = Joi.object({
    name: Joi.string().min(5).max(35).required(),
    role: Joi.string().valid('CPADMINISTRATOR', 'BCTURNKY').optional(),
    email: Joi.string().email().required(),
    enterpriseSid: Joi.string().min(32).max(32),
    password: passwordComplexity(complexityOptions),
    confirmPassword: Joi.any().valid(Joi.ref('password')).required(),
    status: Joi.string().valid('ACTIVE', 'INACTIVE').optional(),
    // accountSid: Joi.string().when('role', {
    //   is: 'BCTURNKY',
    //   then: Joi.string().required(),
    //   otherwise: Joi.string().optional(),
    // }),
  });
  const { error, value } = Schema.validate(req.body);
  if (error) return next(error);
  next();
};

exports.updateUser = (req, res, next) => {
  const Schema = Joi.object({
    name: Joi.string().min(5).max(35),
    role: Joi.string().valid('CPADMINISTRATOR', 'BCTURNKY').optional(),
    enterpriseSid: Joi.string().min(32).max(32),
    email: Joi.string().email(),
  });
};
