const Joi = require('joi');
const passwordComplexity = require('joi-password-complexity');

const complexityOptions = {
  min: 5,
  max: 250,
  lowerCase: 1,
  upperCase: 1,
  numeric: 1,
  symbol: 1,
  requirementCount: 2,
};

exports.createEnterprise = (req, res, next) => {
  const Schema = Joi.object({
    name: Joi.string().min(5).max(30).required(),
    status: Joi.string().valid('ACTIVE', 'INACTIVE').optional(),
    enterpriseAdmin: Joi.object({
      email: Joi.string().email({ tlds: { allow: ['in', 'com'] } }),
      name: Joi.string().min(5).max(30).required(),
      password: passwordComplexity(complexityOptions)
        .required()
        .messages({ mesage: '' }),
      confirmPassword: Joi.any().valid(Joi.ref('password')).required(),
    }),
  });

  const { error, value } = Schema.validate(req.body);
  if (error) return next(error);
  next();
};
