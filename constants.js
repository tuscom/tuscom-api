const prefix = Object.seal({
  CPADMINISTRATOR: 'AC',
  BCTURNKY: 'AAE',
  ENTERPRISE: 'EN',
});

module.exports = prefix;
