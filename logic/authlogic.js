const random = require('../utils/createRandom');
const prefix = require('../constants');
const User = require('../model/usermodel');
const jwt = require('jsonwebtoken');
const { promisify } = require('util');
const logger = require('../logger/winstonlogging');

exports.createUser = async function (data) {
  let sid;
  if (data.role == 'CPADMINISTRATOR') {
    sid = random.createRandomString(prefix.CPADMINISTRATOR);
  } else {
    sid = random.createRandomString(prefix.BCTURNKY);
  }
  logger.info('creating user');
  return await User.create({
    sid,
    name: data.name,
    role: data.role,
    email: data.email,
    enterpriseSid: data.enterpriseSid,
    password: data.password,
    confirmPassword: data.confirmPassword,
  });
};

exports.createJwtTocken = async (id) => {
  logger.info('creating the jwt token');
  return await promisify(jwt.sign)(id, process.env.JWT_SECRETE);
};
