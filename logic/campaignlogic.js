//third party modules
const csv = require('csvtojson');
const StompJs = require('@stomp/stompjs');
const dayjs = require('dayjs');
const utc = require('dayjs/plugin/utc');

Object.assign(global, { WebSocket: require('websocket').w3cwebsocket });

let toNumbers;
let stompClient;
let fromNumbers;
const readFromCsv = async function (filePath) {
  return await csv().fromFile(filePath);
};

const createStompClientForAmq = function () {
  return new StompJs.Client({
    brokerURL: 'ws://localhost:61614/ws',
  });
};

exports.publishToQueue = async function (req) {
  const { toFile, fromFile } = req.files;
  const { fromPrefix, content, channel, startTime } = req.body;
  stompClient = createStompClientForAmq();
  toNumbers = await readFromCsv(toFile[0].path);
  if (fromFile) fromNumbers = await readFromCsv(fromFile[0].path);
  else fromNumbers = `${fromPrefix}${process.envTWILIO_NUMBER}`;
  stompClient.activate();
  console.log('STOMP client activated');
  stompClient.onConnect = (frame) => {
    console.log('STOMP client connected');
    publish(toNumbers, fromNumbers, content, startTime);
  };
};

function publish(data, fromNumbers, message, startTime) {
  if (data.length == 0) {
    console.log('de-activating stomp client');
    stompClient.deactivate();
    return;
  }
  let row = data.shift();
  let fromNumber;
  const fromAddress = { ...fromNumbers };
  if (fromNumbers.length > 0) {
    fromNumber = fromNumbers.shift();
  } else {
    fromNumbers = { ...fromAddress };
    fromNumber = fromNumbers.shift();
  }

  let mqdata = {
    to: row['numbers'],
    from: fromNumber['numbers'],
    body: message,
  };

  stompClient.publish({
    destination: '/queue/foo.bar',
    body: JSON.stringify(mqdata),
    headers: {
      'content-type': 'application/json',
      AMQ_SCHEDULED_DELAY: startTime ? scheduleInMiliseconds(startTime) : 0,
    },
  });
  //using re-cursive method to execute completely
  publish(data, message);
}

const scheduleInMiliseconds = (startTime) => {
  dayjs.extend(utc);
  if (startTime.split('T').length !== 2) {
    startTime = `${startTime}T00:00:00`;
  }
  return dayjs(startTime).diff(dayjs());
};
