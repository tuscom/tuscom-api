const bcrypt = require('bcryptjs');
const mongoose = require('mongoose');
const { isEmail, isAlpha, isAlphanumeric } = require('validator');

const userSchema = new mongoose.Schema(
  {
    sid: {
      type: String,
      required: [true, '{PATH} must be mandatory'],
      validate: isAlphanumeric,
      unique: true,
      minlength: 32,
      maxlength: 32,
      trim: true,
    },
    name: {
      type: String,
      required: [true, 'name is required'],
    },
    role: {
      type: String,
      enum: ['CPADMINISTRATOR', 'BCTURNKY'],
      default: 'BCTURNKY',
    },
    //emailid entityid, enterpriseiD
    email: {
      type: String,
      validator: isEmail,
      unique: true,
      required: [true, 'email id is damn very much required'],
    },
    enterpriseSid: {
      type: String,
      required: function () {
        return this.role === 'BCTURNKY';
      },
      validate: {
        validator: function (value) {
          if (value == 'CPADMINISTRATOR') {
            return false;
          }
        },
        message: "CPADMINISTRATOR shouldn't have enterprise id ",
      },
    },
    password: {
      type: String,
      select: false,
    },
    confirmPassword: {
      type: String,
      validate: function (value) {
        return this.password === value;
      },
      select: false,
    },
    status: {
      type: String,
      enum: ['ACTIVE', 'INACTIVE', 'CLOSED'],
      default: 'ACTIVE',
    },
    accountSid: {
      type: String,
      trim: true,
      required: function (value) {
        return this.role === 'BCTURNKY';
      },
    },
  },
  {
    timestamps: { dateCreated: 'created_at', dateUpdated: 'updated_at' },
  }
);

userSchema.pre('save', async function (next) {
  if (!this.isModified('password')) return next();
  this.password = await bcrypt.hash(this.password, 12);
  this.confirmPassword = undefined;
  next();
});

userSchema.methods.isCorrectPassword = async function (
  userPassword,
  databasePassword
) {
  return await bcrypt.compare(userPassword, databasePassword);
};

userSchema.pre(/^find|^update/, function (next) {
  this.find({ status: { $ne: 'CLOSED' } });
  next();
});
//userSchema.post('save', function (error, doc, next) {});

const User = mongoose.models['User'] || mongoose.model('User', userSchema);

module.exports = User;
