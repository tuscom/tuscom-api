const mongoose = require('mongoose');
const { isAlphanumeric, isAlpha, isEmail } = require('validator');

const enterpriseSchema = new mongoose.Schema(
  {
    sid: {
      type: String,
      required: [true, 'SID is always required'],
      validate: isAlphanumeric,
      trim: true,
    },
    accountsid: {
      type: String,
      required: [true, 'accSid is very much required'],
      //validate: isEmail,
      trim: true,
    },
    name: {
      type: String,
      unique: true,
      required: [true, 'enterprisename is mandatory'],
      // validate: function (value) {
      //   return value.split(' ').every(isAlpha);
      // },
      trim: true,
      unique: true,
    },
    status: {
      type: String,
      enum: ['ACTIVE', 'INACTIVE', 'CLOSED'],
      default: 'ACTIVE',
    },
    enterpriseAdmin: {
      type: String,
      ref: 'User',
      required: [true, 'enterpriseAdmin is a mandatory filed'],
      trim: true,
    },
  },
  {
    timestamps: { dateCreated: 'createdAt', dateUpdated: 'updatedAt' },
    toJSON: { virtuals: true },
    toObject: { virtuals: true },
  }
);

enterpriseSchema.pre(/^find|^update/, function (next) {
  this.find({ status: { $ne: 'CLOSED' } });
  next();
});

enterpriseSchema.pre('aggregate', function (next) {
  this.pipeline().unshift({ $match: { status: { $ne: 'CLOSED' } } });
  next();
});

module.exports = mongoose.model('Enterprise', enterpriseSchema);
