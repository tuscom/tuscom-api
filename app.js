//Third party module
const express = require('express');
const morgan = require('morgan');
const logger = require('./logger/winstonlogging');
const session = require('express-session');
const redis = require('redis');
const passport = require('passport');
let RedisStore = require('connect-redis')(session);

let redisClient = redis.createClient({
  host: 'redis',
  port: 6379,
});

//doveloper module:
const enterpriseRouter = require('./controller/enterpriseController');
const authRouter = require('./controller/authController');
const errorHandler = require('./handler/errorHandler');
const campaginController = require('./controller/campaignController');
const userRouter = require('./controller/userController');

const app = express();

//app.use(morgan('combined', { stream: logger.stream }));

app.use(
  session({
    store: new RedisStore({ client: redisClient }),
    secret: process.env.SESSION_SECRETE,
    cookie: {
      secure: false,
      httpOnly: true,
      maxAge: 24 * 60 * 60 * 1000,
    },
    saveUninitialized: false,
    resave: false,
  })
);

app.use(passport.initialize());
app.use(passport.session());

app.use(express.json());

app.use('/campaignmanager/campaign', campaginController);

app.use('/campaignmanager/enterprise', enterpriseRouter);

app.use('/campaignmanager/users', userRouter);

app.use('/campaignmanager/auth', authRouter);

// setup the logger
app.use(errorHandler);

module.exports = app;
