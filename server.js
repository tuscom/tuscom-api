const User = require('./model/usermodel');
const Enterprise = require('./model/enterpriseModel');
require('./db');
const app = require('./app');

const PORT = process.env.PORT || 6000;

const server = app.listen(PORT, () => {
  console.log(`listening to the port ${PORT}`);
});

process.on('unhandledRejection', () => {
  server.close(() => {
    process.exit(1);
  });
});

process.on('uncaughtException', function () {
  server.close(() => {
    process.exit(1);
  });
});

process.on('SIGTERM', function () {
  server.close(() => {
    console.log('process is terminate');
  });
});

// const deleteDatabase = async () => {
//   await User.deleteMany();
//   await Enterprise.deleteMany();
// };

//deleteDatabase();
// [`exit`, `SIGINT`, `SIGUSR1`, `SIGUSR2`, `uncaughtException`, `SIGTERM`].forEach((eventType) => {
//   process.on(eventType, cleanUp.bind(null, eventType));
// })

console.log(process.env.NODE_ENV);
