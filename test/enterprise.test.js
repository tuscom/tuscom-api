//third party module
const path = require('path');
const appRoot = require('app-root-path');
const request = require('supertest');
const mongoose = require('mongoose');
require('dotenv').config({ path: path.join(appRoot.path, 'config/dev.env') });

//developer module:
const app = require('../app');
const Enterprise = require('../model/enterpriseModel');
const User = require('../model/usermodel');
const {
  createandGetEnterpriseSid,
  getToken,
  connectToDatabase,
  deleteEnterpriseCollection,
  deleteUsersCollection,
} = require('./setup');

let token;
let db;
let enterpriseSid;

const updateData = {
  name: 'anna bond',
  status: 'INACTIVE',
};

beforeAll(async () => {
  db = await connectToDatabase();
  await deleteEnterpriseCollection();
  await deleteUsersCollection();
  token = await getToken();
  enterpriseSid = await createandGetEnterpriseSid(token);
}, 50000);

const enterpriseData = {
  name: 'why i am not able to',
  enterpriseAdmin: {
    email: 'haramidamnshetty51@gmail.com',
    password: 'abcdabcdA1@',
    name: 'adani the king',
    confirmPassword: 'abcdabcdA1@',
  },
};

test('create enterprise', async function () {
  const response = await request(app)
    .post('/campaignmanager/enterprise')
    .send(enterpriseData)
    .set('Authorization', `Bearer ${token}`);
  const result = await Enterprise.findOne({ sid: response.body.data.sid });
  expect(response.body.code).toBe(200);
  expect(result).not.toBe(null);
});

test('update Enterprise', async function () {
  const response = await request(app)
    .put(`/campaignmanager/enterprise/${enterpriseSid}`)
    .set('Authorization', `Bearer ${token}`)
    .send(updateData);
  const result = await Enterprise.findOne({ sid: enterpriseSid });
  expect(result.name).toBe(updateData.name);
  expect(result.status).toBe(updateData.status);
});

test('get Enterpise', async function () {
  const response = await request(app)
    .get(`/campaignmanager/enterprise/search/${enterpriseSid}`)
    .set('Authorization', `Bearer ${token}`);
  expect(response.body.data.sid).toBe(enterpriseSid);
  expect(response.body.code).toBe(200);
});

test('Get All Enterprise', async function () {
  const response = await request(app)
    .get('/campaignmanager/enterprise/search')
    .set('Authorization', `Bearer ${token}`);
  expect(response.body.code).toBe(200);
  expect(response.body.data.result).toEqual(
    expect.arrayContaining([expect.objectContaining({ sid: enterpriseSid })])
  );
});

afterAll(async () => {
  await User.deleteMany();
  await Enterprise.deleteMany();
  db.disconnect();
});
