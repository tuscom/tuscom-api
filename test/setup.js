//node core module
const path = require('path');
const appRoot = require('app-root-path');

const mongoose = require('mongoose');
const request = require('supertest');

require('dotenv').config({ path: path.join(appRoot.path, 'config/dev.env') });

const app = require('../app');
const User = require('../model/enterpriseModel');
const Enterpise = require('../model/usermodel');

const DB = process.env.DATABASE_CLOUD.replace(
  '<PASSWORD>',
  process.env.DATABASE_PASSWORD
);

const testEnterpriseData = {
  name: 'i am for testing only',
  enterpriseAdmin: {
    email: 'pleasetestme@gmail.com',
    password: 'abcdabcdA1@',
    name: 'adani doom',
    confirmPassword: 'abcdabcdA1@',
  },
};

const testUser = {
  name: 'kajshdgasdkhlaskd',
  role: 'CPADMINISTRATOR',
  email: 'damndamnn@gmail.com',
  password: 'nononononA2@',
  confirmPassword: 'nononononA2@',
};

async function connectToDatabase() {
  return await mongoose.connect(DB);
}

async function deleteUsersCollection() {
  await User.deleteMany();
}

async function deleteEnterpriseCollection() {
  await Enterpise.deleteMany();
}

async function createandGetEnterpriseSid(token) {
  const response = await request(app)
    .post('/campaignmanager/enterprise')
    .send(testEnterpriseData)
    .set('Authorization', `Bearer ${token}`);
  return response.body.data.sid;
}

async function getToken() {
  const response = await request(app)
    .post('/campaignmanager/auth/signup')
    .send(testUser);
  return response.body.token;
}

module.exports = {
  getToken,
  createandGetEnterpriseSid,
  connectToDatabase,
  deleteEnterpriseCollection,
  deleteUsersCollection,
};
