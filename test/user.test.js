//node core module
const path = require('path');

//third party module
const appRoot = require('app-root-path');
const mongoose = require('mongoose');
const request = require('supertest');

require('dotenv').config({ path: path.join(appRoot.path, 'config/dev.env') });

//developer module
const app = require('../app');
const User = require('../model/usermodel');
const {
  createandGetEnterpriseSid,
  getToken,
  connectToDatabase,
  deleteEnterpriseCollection,
  deleteUsersCollection,
} = require('./setup');
const { updateUser } = require('../handler/userHandler');

let token;
let db;
let enterpriseSid;
let userSid;

beforeAll(async function () {
  db = await connectToDatabase();
  await deleteEnterpriseCollection();
  await deleteUsersCollection();
  token = await getToken();
  enterpriseSid = await createandGetEnterpriseSid(token);
}, 50000);

test('create user', async function () {
  const user = {
    name: 'my name is khan',
    email: 'idnotno@gmail.com',
    password: 'Abcd1234@',
    role: 'BCTURNKY',
    enterpriseSid,
    confirmPassword: 'Abcd1234@',
  };
  const response = await request(app)
    .post('/campaignmanager/users')
    .send(user)
    .set('Authorization', `Bearer ${token}`);
  const result = await User.findOne({ sid: response.body.data.sid });
  expect(result).not.toBe(null);
  expect(result.enterpriseId).toBe(user.enterpriseId);
  userSid = response.body.data.sid;
});

const userUpdate = {
  name: 'alhfjkads',
  enterpriseSid: 'ENe441720c45c5970a38b281399694b9',
};

test('update user', async function () {
  const response = await request(app)
    .post('/campaignmanager/users')
    .set('Authorization', `Bearer ${token}`)
    .send(userUpdate);
  console.log(response.body);
});

test('get All users', async function () {
  const response = await request(app).get('/campaignmanager/users/search');
  expect(response.body.code).toBe(200);
  expect(response.body.data.result).toEqual(
    expect.arrayContaining([expect.objectContaining({ sid: userSid })])
  );
});

afterAll(async function () {
  await User.deleteMany();
  db.disconnect();
});
