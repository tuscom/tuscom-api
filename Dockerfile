FROM node:14.19.0-alpine
WORKDIR /app
COPY package.json ./
ARG NODE_ENV
RUN if [ "${NODE_ENV}" == 'development' ]; \
        then npm install; \
        else npm install --only=production; \
        fi
COPY . ./
EXPOSE 6000
CMD ["node" , "server.js"]