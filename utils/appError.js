class AppError extends Error {
  constructor(message, statusCode, errorcode, data) {
    super(message);
    this.statusCode = statusCode;
    this.errorcode = errorcode;
    this.data = data;
    Error.captureStackTrace(this, this.constructor);
  }
}

module.exports = AppError;
