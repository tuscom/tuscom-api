const mailgun = require('mailgun-js');
const mail = mailgun({
  apiKey: process.env.MAILGUN_API_KEY,
  domain: process.env.MAILGUN_DOMAIN,
});

class Email {
  constructor(user) {
    this.to = user.email;
    this.from = 'sachinkshetty41@gmail.com';
    this.name = user.username;
  }
  async send(subject, message) {
    try {
      await mail.messages().send({
        from: this.from,
        to: this.to,
        subject,
        text: message,
      });
    } catch (err) {
      throw err;
    }
  }

  async sendWelcome() {
    await this.send(
      'welcome',
      `hi ${this.name} welcome to the enterprise family`
    );
  }
}

module.exports = Email;
