const crypto = require('crypto');

exports.createRandomString = (prefix) => {
  const random = crypto.randomBytes(15).toString('hex');
  return `${prefix}${random}`.substring(0, 32);
};
