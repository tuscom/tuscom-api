class APIfeature {
  constructor(queryStr) {
    this.queryStr = queryStr;
  }

  filter() {
    let query = { ...this.queryStr };

    const startTime = query.startTime;
    const endTime = query.endTime;

    //quering based on startDate and endDate
    if (startTime && endTime)
      query.createdAt = {
        $gte: { $regex: startTime },
        $lte: { $regex: endTime },
      };
    else if (startTime) query.createdAt = { $gte: { $regex: startTime } };
    else if (endTime) query.createdAt = { $lte: { $regex: endTime } };

    const allowedFeilds = ['name', 'Status', 'startTime', 'endTime'];

    query = allowedFeilds.reduce((acc, el) => {
      acc[el] = query[el];
      return acc;
    }, {});
    //replacing $ for query string properties
    let strquery = JSON.stringify(query);
    strquery = strquery.replace(/\b(gte|gt|lte|lt)\b/g, (match) => `$${match}`);
    return JSON.parse(strquery);
  }

  sort() {
    if (!this.queryStr.sortBy) return { createdAt: -1 };
    const sortby = this.queryStr.sortBy.split(',').reduce((acc, curr) => {
      curr.startsWith('-') ? (acc[curr] = -1) : (acc[curr] = 1);
      return acc;
    }, {});
    return sortby;
  }

  pagination() {
    const page = this.queryStr.page;
    let skip;
    let pageSize = this.queryStr.pageSize;
    if (!pageSize) return { skip: 0, pageSize: 10 };
    if (pageSize < 1)
      throw new AppError('page size should not be less than 1', 409, 5000);
    skip = page * pageSize;

    return { skip, pageSize };
  }

  createUri(originalUrl) {
    const data = { ...this.queryStr };
    return Object.keys(data).reduce((acc, curr) => {
      if (!acc.includes('?')) return `${acc}?${curr}=${data[curr]}`;
      return `${acc}&${curr}=${data[curr]}`;
    }, originalUrl);
  }

  firstPageUri(originalUrl) {
    const data = { ...this.queryStr };
    data.page = 0;
    data.pageSize = data.pageSize || 10;
    return Object.keys(data).reduce((acc, curr) => {
      //if (curr == 'page' || curr == 'pageSize') return acc;
      if (!acc.includes('?')) return `${acc}?${curr}=${data[curr]}`;
      return `${acc}&${curr}=${data[curr]}`;
    }, originalUrl);
  }

  nextPageUri(originalUrl) {
    const data = { ...this.queryStr };
    data.page = data.page ? data.page + 1 : 1;
    data.pageSize = data.pageSize || 10;
    return Object.keys(data).reduce((acc, curr) => {
      if (acc.includes('?')) return `${acc}?${curr}=${data[curr]}`;
      return `${acc}&${curr}=${data[curr]}`;
    }, originalUrl);
  }
}

module.exports = APIfeature;
