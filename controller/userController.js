//third party module
const express = require('express');

//developer module
const authHandler = require('../handler/authHandler');
const handler = require('../handler/userHandler');
const validate = require('../validation/userValidation');

const router = express.Router();

router
  .route('/')
  .post(
    validate.createUser,
    authHandler.protect,
    authHandler.restrictTo('CPADMINISTRATOR'),
    handler.createUser
  );

router.route('/search').get(handler.getAllUser);

router.route('/search/:id').get(handler.getUser);

router.route('/:sid').put(authHandler.protect, handler.updateUser);

module.exports = router;
