//third party module
const express = require('express');

//doveloper module
const handler = require('../handler/enterpriseHandler');
const authHandler = require('../handler/authHandler');
const validate = require('../validation/enterpriseValidation');

const router = express.Router();

router.use(authHandler.protect);

router
  .route('/')
  .post(
    validate.createEnterprise,
    authHandler.restrictTo('CPADMINISTRATOR'),
    handler.createEnterprise
  );

router.route('/:sid').put(handler.updateEnterprise);

router.route('/search').get(handler.getAll);
router
  .route('/search/:sid')
  .get(handler.getOneEnterprise)
  .delete(handler.deleteEnterprise);

module.exports = router;
