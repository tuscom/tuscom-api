//third party module
const express = require('express');
const passport = require('passport');

//doveloper module
const handler = require('../handler/authHandler');
require('../factory/passportFactory');

const router = express.Router();

router.route('/login').post(handler.login);

router.route('/loginsession', passport.authenticate('local'));

router.route('/signup').post(handler.signUp);

module.exports = router;
