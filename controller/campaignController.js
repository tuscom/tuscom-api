//third party module
const express = require('express');

//developer module
const handler = require('../handler/campain');

const router = express.Router();

router.route('/').post(handler.uploadFile, handler.createCampaign);

module.exports = router;
