require('dotenv').config({ path: './config/dev.env' });
const twilio = require('twilio')(
  process.env.TWILIO_ACCOUNT_SID,
  process.env.TWILIO_AUTH_TOKEN
);
const fs = require('fs').promises;
const approotpath = require('app-root-path');
const path = require('path');

const readFile = async function () {
  let data = await fs.readFile(
    path.join(approotpath.path, 'numbers.csv'),
    'utf-8'
  );
  data = data.split(';\r\n');
  data.pop();
  return data;
};

const sendmessage = async function () {
  const numbers = await readFile();
  try {
    const result = await Promise.all(
      numbers.map((number) => {
        return twilio.messages.create({
          to: number,
          from: process.env.TWILIO_MESSAGING_SERVICE_SID,
          body: 'go and die or do or die',
        });
      })
    );
    console.log(result);
  } catch (err) {
    console.log(err);
  }
};

sendmessage();

// const sendingsms = async function () {
//   const service = twilio.notify.services(process.env.TWILIO_NOTIFY_SERVICE_SID);
//   const numbers = await readFile();
//   const bindings = numbers.map((number) =>
//     JSON.stringify({ binding_type: 'sms', address: number })
//   );
//   try {
//     const result = await service.notifications.create({
//       toBinding: bindings,
//       body: 'my name is khan',
//     });
//     console.log(result);
//   } catch (err) {
//     console.log(err);
//   }
// };

// sendingsms();
