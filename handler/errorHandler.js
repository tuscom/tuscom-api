const AppError = require('../utils/appError');

const handleDuplicateError = (err) => {
  const value = err.errmsg.match(/(["'])(\\?.)*?\1/);
  const a = err.errmsg;
  const data = err.keyValue;
  //const obj = a.substring(a.indexOf('{'), a.indexOf('}') + 1);
  //const key = obj.substring(obj.indexOf('{') + 1, obj.indexOf(':')).trim();
  if (a.includes('enterprises')) {
    return new AppError('Enterprise name is not Unque', 409, 4050, data);
  }
  if (a.includes('users')) {
    return new AppError('User already exists', 400, 4001, data);
  }

  const message = `Duplicate field value: ${value}. Please use another value!`;
  return new AppError(message, 409, 4050);
};

const handleValidationErrorDB = (err) => {
  const error = Object.values(err.errors).map((el) => el.message);
  const message = `invalid input ${error.join('. ')}`;
  return new AppError(message);
};

module.exports = (err, req, res, next) => {
  let error = { ...err };
  error.message = err.message;
  error.errmsg = err.errmsg;
  error.statusCode = error.statusCode || 500;
  error.errorcode = error.errorcode || 5000;
  if (error.code === 11000) error = handleDuplicateError(error);
  res.status(error.statusCode).json({
    code: error.errorcode,
    message: error.message,
    data: error.data,
  });
};
