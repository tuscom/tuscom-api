//thirdparty modules
const multer = require('multer');
const appRootPath = require('app-root-path');
const path = require('path');

//developer module
const AppError = require('../utils/appError');
const producer = require('../logic/campaignlogic');

const storage = multer.diskStorage({
  destination: function (req, file, done) {
    done(null, path.join(appRootPath.path, 'files'));
  },
  filename: function (req, file, done) {
    const ext = file.mimetype.split('/')[1];
    done(null, `${file.fieldname}-${Date.now()}.${ext}`);
  },
});

const fileFilter = (req, file, done) => {
  if (file.mimetype.split('/')[1] == 'csv') {
    done(null, true);
  } else {
    done(new AppError('not a valid file', 409), false);
  }
};

const upload = multer({
  storage,
  fileFilter,
});

exports.uploadFile = upload.fields([
  { name: 'fromFile', maxCount: 1 },
  { name: 'toFile', maxCount: 1 },
]);

exports.createCampaign = async (req, res, next) => {
  try {
    await producer.publishToQueue(req);
    res.status(200).json({
      file: req.files,
      body: req.body,
    });
  } catch (err) {
    next(err);
  }
};
