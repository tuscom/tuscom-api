const Enterprise = require('../model/enterpriseModel');
const User = require('../model/usermodel');
const sid = require('../utils/createRandom');
const db = require('mongoose');
//const Email = require('../utils/email');
const prefix = require('../constants');
const APIfeature = require('../utils/apiFeatures');
const AppError = require('../utils/appError');

exports.createEnterprise = async (req, res, next) => {
  const enterpriseSID = sid.createRandomString(prefix.ENTERPRISE);
  const enterpriseAdminSID = sid.createRandomString(prefix.BCTURNKY);
  const data = req.body;
  const adminData = data.enterpriseAdmin;
  const enterpriseData = {
    sid: enterpriseSID,
    name: data.name,
    enterpriseAdmin: enterpriseAdminSID,
    accountsid: req.user.sid,
  };
  const enterpriseAdminData = {
    sid: enterpriseAdminSID,
    name: adminData.name,
    email: adminData.email,
    password: adminData.password,
    enterpriseSid: enterpriseSID,
    accountSid: req.user.sid,
  };
  const session = await db.startSession();
  try {
    session.startTransaction();

    // await new User(enterpriseAdminData).save({ session });
    // await new Enterprise(enterpriseData).save({ session });
    const enterprise = await new Enterprise(enterpriseData).save({ session });
    const userAdmin = await new User(enterpriseAdminData).save({ session });

    await session.commitTransaction();

    //await new Email(userAdmin).sendWelcome();
    res.status(200).json({
      data: {
        sid: enterprise.sid,
        dateUpdated: enterprise.updatedAt,
        dateCreated: enterprise.createdAt,
        accountSid: req.user.sid,
        accountEmail: enterprise.accountEmail,
        uri: `${req.originalUrl}/search/enterprise/${enterprise.sid}`,
        name: enterprise.name,
        status: enterprise.status,
        enterpriseAdminSid: userAdmin.sid,
        enterpriseAdminEmail: userAdmin.email,
      },
      code: 200,
      message: 'OK',
    });
  } catch (err) {
    await session.abortTransaction();
    next(err);
  } finally {
    session.endSession();
  }
};

exports.getAll = async (req, res, next) => {
  const features = new APIfeature(req.query);
  try {
    const enterprises = await Enterprise.aggregate([
      {
        $facet: {
          total: [
            {
              $count: 'total',
            },
          ],

          data: [
            {
              $match: features.filter(),
            },
            {
              $sort: features.sort(),
            },
            {
              $skip: features.pagination().skip,
            },
            {
              $limit: features.pagination().pageSize,
            },
            {
              $lookup: {
                from: 'users',
                localField: 'enterpriseAdmin',
                foreignField: 'sid',
                as: 'enterpriseAdmin',
              },
            },
            {
              $project: {
                sid: 1,
                accountEmail: 1,
                dateCreated: '$createdAt',
                dateUpdated: '$updatedAt',
                name: '$name',
                emailId: { $arrayElemAt: ['$enterpriseAdmin.emailId', 0] },
                status: 1,
                role: { $arrayElemAt: ['$enterpriseAdmin.role', 0] },
              },
            },
          ],
        },
      },
    ]);
    let data;
    if (enterprises[0].total.length < 1) {
      data = { meassage: 'no data found for the given search' };
    } else {
      const total = enterprises[0].total[0].total;
      const pageSize = features.pagination().pageSize;
      const page = req.query.page || 0;
      const start = page * pageSize;
      const numPages = Math.ceil(total / pageSize);
      data = {
        result: enterprises[0].data,
        pageSize,
        total,
        page,
        numPages,
        start,
        end: start + (pageSize - 1),
        firstPageUri:
          numPages > 1 ? features.firstPageUri(req.originalUrl) : undefined,
        nextPageUri:
          numPages - 1 == page
            ? undefined
            : features.nextPageUri(req.originalUrl),
        uri: features.createUri(req.originalUrl),
      };
    }

    res.status(200).json({
      data,
      code: 200,
      message: 'OK',
    });
  } catch (err) {
    console.log(err.stack);
    next(err);
  }
};

exports.getOneEnterprise = async (req, res, next) => {
  const sid = req.params.sid;
  const enterprise = await Enterprise.aggregate([
    {
      $match: { sid: sid },
    },
    {
      $lookup: {
        from: 'users',
        localField: 'accountsid',
        foreignField: 'sid',
        as: 'cp',
      },
    },
    {
      $lookup: {
        from: 'users',
        localField: 'enterpriseAdmin',
        foreignField: 'sid',
        as: 'enterpriseAdmin',
      },
    },
    {
      $project: {
        _id: 0,
        sid: 1,
        dateUpdated: '$updatedAt',
        dateCreated: '$createdAt',
        accountSid: { $arrayElemAt: ['$cp.sid', 0] },
        accountEmail: { $arrayElemAt: ['$cp.email', 0] },
        uri: `${req.originalUrl}/${sid}`,
        name: '$enterpriseName',
        status: '$status',
        enterpriseAdminSid: { $arrayElemAt: ['$enterpriseName.sid', 0] },
        enterpriseAdminEmail: { $arrayElemAt: ['$enterpriseName.email', 0] },
      },
    },
  ]);
  if (enterprise.length === 0)
    return next(new AppError('Enterprise not found', 409, 4051));
  res.status(200).json({
    data: enterprise[0],
    meassage: 'OK',
    code: 200,
  });
};

exports.deleteEnterprise = async (req, res, next) => {
  let session;
  const sid = req.params.sid;
  try {
    console.log('starting session');
    session = await db.startSession();
    console.log('starting transactions');
    session.startTransaction();
    console.log('updating the enterprise');
    const enterprise = await Enterprise.findOneAndUpdate(
      { sid },
      { $set: { status: 'CLOSED' } },
      { session, new: true }
    );
    if (!enterprise)
      return next(new AppError('Enterprise not found', 409, 4051, { sid }));
    console.log('enterprise updated');

    console.log('updating the users');
    await User.updateMany(
      { enterprise: sid },
      { $set: { status: 'CLOSED' } },
      { session }
    );
    console.log('user updated');
    console.log('committing transactions');
    await session.commitTransaction();
    const adminUser = await User.findOne({ sid: enterprise.enterpriseAdmin });

    res.status(200).json({
      data: {
        sid: enterprise.sid,
        dateUpdated: enterprise.updatedAt,
        dateCreated: enterprise.createdAt,
        accountSid: req.user.sid,
        accountEmail: req.user.email,
        uri: `${req.originalUrl}/${sid}`,
        name: enterprise.name,
        status: enterprise.status,
        enterpriseAdminSID: enterprise.enterpriseAdmin,
        enterpriseAdminEmail: adminUser.email,
      },
      code: 200,
      meassage: 'OK',
    });
  } catch (err) {
    console.log('aborting transactions');
    await session.abortTransaction();
    console.log(err);
  } finally {
    console.log('ending the session');
    session.endSession();
  }
};

exports.updateEnterprise = async (req, res, next) => {
  const sid = req.params.sid;
  const data = { ...req.body };
  const allowedFeilds = ['name', 'status'];
  try {
    Object.keys(data).forEach((ele) => {
      if (!allowedFeilds.includes(ele)) delete data[ele];
    });
    const enterprise = await Enterprise.findOne({ sid });
    if (!enterprise)
      return next(new AppError('Enterprise not found', 409, 4051));
    Object.keys(data).forEach((ele) => (enterprise[ele] = data[ele]));
    await enterprise.save();
    const adminUser = await User.findOne({ sid: enterprise.enterpriseAdmin });
    res.status(201).json({
      data: {
        sid: enterprise.sid,
        dateCreated: enterprise.updatedAt,
        dateCreated: enterprise.createdAt,
        accountSid: req.user.sid,
        accountEmail: req.user.email,
        uri: `${req.originalUrl}/search/${enterprise.sid}`,
        name: enterprise.name,
        status: enterprise.status,
        enterpriseAdminSid: enterprise.enterpriseAdmin,
        enterpriseAdminEmail: adminUser.email,
      },
      code: 200,
      meassage: 'OK',
    });
  } catch (err) {
    next(err);
  }
};
