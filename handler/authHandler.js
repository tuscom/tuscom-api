const jwt = require('jsonwebtoken');
const User = require('../model/usermodel');
const AppError = require('../utils/appError');
const { promisify } = require('util');
const logic = require('../logic/authlogic');
const logger = require('../logger/winstonlogging');

exports.login = async (req, res, next) => {
  const { username, password } = req.body;
  try {
    const user = await User.findOne({
      $or: [{ email: username }, { username }],
    }).select('+password');
    if (!user || !user.isCorrectPassword(password, user.password)) {
      return res.status(401).json({
        message: 'Unathorized',
      });
    }
    const token = await promisify(jwt.sign)(
      { email: username },
      process.env.JWT_SECRETE,
      {
        expiresIn: process.env.JWT_EXPIRES_IN,
      }
    );

    res.status(200).json({
      status: 'success',
      token,
    });
  } catch (err) {
    next(err);
  }
};

exports.protect = async (req, res, next) => {
  let token;
  try {
    if (
      req.headers.authorization &&
      req.headers['authorization'].startsWith('Bearer')
    ) {
      token = req.headers['authorization'].split(' ')[1];
    }

    if (!token)
      return next(new AppError('your not logged in please login', 401));

    const payload = await promisify(jwt.verify)(token, process.env.JWT_SECRETE);

    const user = await User.findOne({
      $or: [{ email: payload.email }, { username: payload.email }],
    });
    if (!user) return next(new AppError('your are not authorised', 401));
    req.user = user;
    next();
  } catch (err) {
    next(err);
  }
};

exports.restrictTo = (...role) => {
  return (req, res, next) => {
    console.log('i did run too');
    if (!role.includes(req.user.role)) {
      return next(new AppError('your not authorised to do this shit', 401));
    }
    next();
  };
};

exports.isAuth = (req, res, next) => {
  if (!req.user)
    return next(new AppError('your not logged in please login', 401));
  return next();
};

exports.signUp = async (req, res, next) => {
  try {
    const user = await logic.createUser(req.body);
    logger.info('user is created');
    const token = await logic.createJwtTocken({ email: user.email });
    logger.info('jwt tocken is created');
    logger.info('sending the response to the client');
    res.status(201).json({
      status: 'success',
      token,
      data: user,
    });
    logger.info('sent the response to the usert');
  } catch (err) {
    logger.error(err);
    next(err);
  }
};
