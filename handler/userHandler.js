const User = require('../model/userModel');
const Enterprise = require('../model/enterpriseModel');
const prefix = require('../constants');
const random = require('../utils/createRandom');
const AppError = require('../utils/appError');
const APIfeature = require('../utils/apiFeatures');

exports.createUser = async (req, res, next) => {
  const data = req.body;
  const role = data.role || 'BCTURNKY';
  let enterpriseSid;
  const accountSid = req.user.sid;
  if (role === 'BCTURNKY') {
    enterpriseSid = data.enterpriseSid;
    if (!enterpriseSid)
      return next(new AppError('Enterprise is missing', 409, 4051));
    const enterprise = await Enterprise.findOne({ sid: enterpriseSid });
    if (!enterprise)
      return next(new AppError('Enterprise not found', 400, 4051));
  }
  const sid = random.createRandomString(prefix[role]);
  try {
    const user = await User.create({
      sid,
      name: data.name,
      email: data.email,
      password: data.password,
      role,
      status: data.status,
      enterpriseSid,
      confirmPassword: data.confirmPassword,
      accountSid,
    });

    res.status(201).json({
      data: {
        sid: user.sid,
        accountEmail: req.user.email,
        dateCreated: user.createdAt,
        dateUpdated: user.updatedAt,
        name: user.name,
        emailID: user.email,
        status: user.status,
        role: user.role,
        enterpriseSid: user.enterpriseSid,
        uri: `${req.originalUrl}/${user.sid}`,
      },
      code: 200,
      message: 'OK',
    });
  } catch (err) {
    next(err);
  }
};

exports.getUser = async (req, res, next) => {
  try {
    const sid = req.params.sid;
    const user = await User.findOne({ sid });
    if (!user) return new AppError('User not found', 409, 4005, { sid });
    res.status(200).json({
      sid: user.sid,
      accountEmail: req.user.email,
      dateCreated: user.createdAt,
      dateUpdated: user.updatedAt,
      name: user.name,
      emailID: user.email,
      status: user.status,
      role: user.role,
    });
  } catch (err) {
    next(err);
  }
};

exports.updateUser = async (req, res, next) => {
  const sid = req.params.sid;
  const data = JSON.parse(JSON.stringify(req.body));
  try {
    const allowedFeilds = ['name', 'role', 'status', 'enterpriseSid', 'email'];
    Object.keys(data).forEach((ele) => {
      if (!allowedFeilds.includes(ele)) delete data[ele];
    });

    if (data.enterpriseSid) {
      const enterprise = await Enterprise.findOne({ sid: data.enterpriseSid });
      if (!enterprise)
        return next(new AppError('', 409, 4051, { sid: data.enterpriseSid }));
      if (data.role) {
        if (data.role !== 'BCTURNKY') data.enterpriseSid = undefined;
      } else {
        const result = await User.findOne({ sid });
        if (result)
          if (result.role !== 'BCTURNKY') data.enterpriseSid = undefined;
      }
    }

    const user = await User.findOneAndUpdate({ sid }, data, {
      new: true,
      runValidators: true,
    });

    if (!user) next(new AppError('User not found', 409, 4005, { data }));
    res.status(200).json({
      sid: user.sid,
      accountEmail: req.user.email,
      dateCreated: user.createdAt,
      dateUpdated: user.updatedAt,
      name: user.name,
      emailID: user.email,
      status: user.status,
      role: user.role,
      enterpriseSid: user.enterpriseSid,
      uri: `${req.originalUrl}/${user.sid}`,
    });
  } catch (err) {
    next(err);
  }
};

const filterObj = (query) => {
  const startTime = query.startTime;
  const endTime = query.endTime;

  let queryString = JSON.stringify(query);
  queryString = queryString.replace(
    /\b(gte|gt|lte|lt)\b/g,
    (match) => `$${match}`
  );

  const queryObj = JSON.parse(queryString);

  if (startTime && endTime) {
    queryObj.createdAt = {
      $gte: { $regex: startTime },
      $lte: { $regex: endTime },
    };
  } else if (startTime) {
    queryObj.createdAt = {
      $gte: { $regex: startTime },
    };
  } else if (endTime) {
    queryObj.createdAt = {
      $lte: { $regex: endTime },
    };
  }

  const allowedFeilds = [
    'name',
    'email',
    'role',
    'enterpriseSid',
    'status',
    'createdAt',
  ];

  Object.keys(queryObj).forEach((ele) => {
    if (!allowedFeilds.includes(ele)) delete queryObj[ele];
  });

  return queryObj;
};

const sort = (query) => {
  if (!query.sortBy) return { createdAt: -1 };
  return query.sortBy.split(',').reduce((acc, curr) => {
    acc[curr] = curr.startsWith('-') ? -1 : 1;
    return acc;
  }, {});
};

const pagination = (query) => {
  const page = query.page || 0;
  const limit = query.pageSize || 10;
  const skip = page * limit;
  return { skip, limit };
};

exports.getAllUser = async (req, res, next) => {
  const search = req.query;
  const features = new APIfeature(search);
  const { skip, limit } = pagination(search);

  try {
    const users = await User.aggregate([
      {
        $facet: {
          total: [
            {
              $count: 'total',
            },
          ],
          data: [
            {
              $match: filterObj(search),
            },
            {
              $sort: sort(search),
            },
            {
              $skip: skip,
            },
            {
              $limit: limit,
            },
            {
              $project: {
                _id: 0,
                sid: 1,
                dateCreated: '$createdAt',
                dateUpdated: '$updatedAt',
                name: '$name',
                emailID: '$email',
                status: '$status',
                role: '$role',
                enterpriseSid: '$enterpriseSid',
                uri: `${req.originalUrl}`,
              },
            },
          ],
        },
      },
    ]);

    const data = users[0].data;
    const pageSize = search.pageSize || 10;
    const total = users[0].total[0].total;
    const page = search.page || 0;
    const numPages = Math.ceil(total / pageSize);
    const start = skip;
    const end = start + (pageSize - 1);
    const firstPageUri = features.firstPageUri(req.originalUrl);
    const uri = features.createUri(req.originalUrl);

    res.status(200).json({
      data: {
        result: data,
        pageSize,
        total,
        page,
        numPages,
        start,
        end,
        firstPageUri,
        uri,
      },
      code: 200,
      message: 'OK',
    });
  } catch (err) {
    next(err);
  }
};
