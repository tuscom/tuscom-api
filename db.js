//third party module import
const mongoose = require('mongoose');

require('dotenv').config({ path: './config/dev.env' });

//doveloper module import:
const logger = require('./logger/winstonlogging');

let DB;
//variable declaration
if (process.env.NODE_ENV !== 'production') {
  console.log('I am cloud DB');
  DB = process.env.DATABASE_CLOUD.replace(
    '<PASSWORD>',
    process.env.DATABASE_PASSWORD
  );
} else {
  DB = process.env.DATABASE;
}

let count = 0;
const connectWithRetry = function () {
  mongoose.connect(DB, (err) => {
    if (err) {
      count++;
      if (count < 5) {
        console.log('i did run too');
        setInterval(retrylogic, process.env.RETRYSECONDS);
      }
    }
  });
};

const db = mongoose.connection;
mongoose.connect(DB);

db.on('connecting', function () {
  logger.info('connecting to the mongodb');
});

db.on('open', function () {
  logger.error(new Error('something definetely went wrong'));
  logger.info('connection to the database is open');
});

db.on('connected', function () {
  console.info('connected to the database');
});

db.on('reconnected', function () {
  console.log('re-connected to the mongodb');
});

db.on('disconnected', connectWithRetry);

db.on('error', function (err) {
  console.log(err);
  console.log('i did run');
  mongoose.disconnect();
});
